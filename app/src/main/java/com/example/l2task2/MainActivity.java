package com.example.l2task2;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Random;
import java.util.Set;

public class MainActivity extends AppCompatActivity {

    TextView firstTextView;
    TextView secondTextView;
    TextView thirdTextView;
    TextView foursTextView;
    Set<String> colorsArray = new HashSet<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        generateColors();

        firstTextView = (TextView) findViewById(R.id.first_text_view);
        firstTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Iterator <String> iterator = colorsArray.iterator();
                Random rn = new Random();
                int random = rn.nextInt((9 - 0) + 1);

                for(int i = 1; i <= random; i++) {
                    iterator.next();
                }
                secondTextView.setBackgroundColor(Color.parseColor(iterator.next()));

                iterator = colorsArray.iterator();
                rn = new Random();
                random = rn.nextInt((9 - 0) + 1);
                for(int i = 1; i < random; i++) {
                    iterator.next();
                }
                thirdTextView.setBackgroundColor(Color.parseColor(iterator.next()));

                iterator = colorsArray.iterator();
                rn = new Random();
                random = rn.nextInt((9 - 0) + 1);
                for(int i = 1; i <= random -1 ; i++) {
                    iterator.next();
                }
                foursTextView.setBackgroundColor(Color.parseColor(iterator.next()));
            }
        });

        secondTextView = (TextView) findViewById(R.id.second_text_view);
        secondTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Iterator <String> iterator = colorsArray.iterator();
                Random rn = new Random();
                int random = rn.nextInt((9 - 0) + 1);

                for(int i = 1; i <= random; i++) {
                    iterator.next();
                }

                firstTextView.setBackgroundColor(Color.parseColor(iterator.next()));

                iterator = colorsArray.iterator();
                rn = new Random();
                random = rn.nextInt((9 - 0) + 1);
                for(int i = 1; i < random; i++) {
                    iterator.next();
                }
                thirdTextView.setBackgroundColor(Color.parseColor(iterator.next()));

                iterator = colorsArray.iterator();
                rn = new Random();
                random = rn.nextInt((9 - 0) + 1);
                for(int i = 1; i <= random -1 ; i++) {
                    iterator.next();
                }
                foursTextView.setBackgroundColor(Color.parseColor(iterator.next()));
            }
        });

        thirdTextView = (TextView) findViewById(R.id.third_text_view);
        thirdTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Iterator <String> iterator = colorsArray.iterator();
                Random rn = new Random();
                int random = rn.nextInt((9 - 0) + 1);

                for(int i = 1; i <= random; i++) {
                    iterator.next();
                }

                firstTextView.setBackgroundColor(Color.parseColor(iterator.next()));

                iterator = colorsArray.iterator();
                rn = new Random();
                random = rn.nextInt((9 - 0) + 1);
                for(int i = 1; i < random; i++) {
                    iterator.next();
                }
                secondTextView.setBackgroundColor(Color.parseColor(iterator.next()));

                iterator = colorsArray.iterator();
                rn = new Random();
                random = rn.nextInt((9 - 0) + 1);
                for(int i = 1; i <= random -1 ; i++) {
                    iterator.next();
                }
                foursTextView.setBackgroundColor(Color.parseColor(iterator.next()));
            }
        });

        foursTextView = (TextView) findViewById(R.id.fours_text_view);
        foursTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Iterator <String> iterator = colorsArray.iterator();
                Random rn = new Random();
                int random = rn.nextInt((9 - 0) + 1);

                for(int i = 1; i <= random; i++) {
                    iterator.next();
                }

                firstTextView.setBackgroundColor(Color.parseColor(iterator.next()));

                iterator = colorsArray.iterator();
                rn = new Random();
                random = rn.nextInt((9 - 0) + 1);
                for(int i = 1; i < random; i++) {
                    iterator.next();
                }
                secondTextView.setBackgroundColor(Color.parseColor(iterator.next()));

                iterator = colorsArray.iterator();
                rn = new Random();
                random = rn.nextInt((9 - 0) + 1);
                for(int i = 1; i <= random -1 ; i++) {
                    iterator.next();
                }
                thirdTextView.setBackgroundColor(Color.parseColor(iterator.next()));
            }
        });

    }

    public void generateColors(){
        colorsArray.add("#dce775");
        colorsArray.add("#ffa726");
        colorsArray.add("#f44336");
        colorsArray.add("#ec407a");
        colorsArray.add("#ab47bc");
        colorsArray.add("#7e57c2");
        colorsArray.add("#5c6bc0");
        colorsArray.add("#e0e0e0");
        colorsArray.add("#4dd0e1");
        colorsArray.add("#4db6ac");
    }

}
